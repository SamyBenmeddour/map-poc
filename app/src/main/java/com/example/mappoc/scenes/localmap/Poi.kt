package com.example.mappoc.scenes.localmap

data class Poi (val codeEtab: String,
                val name: String,
                val activity: String,
                val visual: String,
                val pin_carto: String,
                val longitude: Double,
                val latitude: Double,
                val isSelected: Boolean = false)
