package com.example.mappoc.scenes.localmap

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.mappoc.ui.views.map.Location
import com.example.mappoc.ui.views.map.router.MapRouter


typealias PoiList = List<Poi>

class LocalMapViewModel (private val router: MapRouter) : ViewModel() {

    val pois: LiveData<PoiList> = MutableLiveData()

    val isDiscoverVisible: LiveData<Boolean> = MutableLiveData(false)

    val isBottomCardVisible: LiveData<Boolean> = Transformations.map(pois) {
        it.firstOrNull { it.isSelected } != null
    }

    val selectedPoi: LiveData<Poi> = Transformations.map(pois) {
        it.firstOrNull { it.isSelected }
    }

    init {
        pois as MutableLiveData
        pois.value = mockResultWithoutQuestion()
    }

    fun poiClicked(poi: Poi) {
        val pois = ArrayList(pois.value!!).map {
            Poi(
                it.codeEtab,
                it.name,
                it.activity,
                it.visual,
                it.pin_carto,
                it.longitude,
                it.latitude,
                if (it.codeEtab == poi.codeEtab) !it.isSelected else false
            )
        }
        (this.pois as MutableLiveData).value = pois
    }

    fun cameraMoved(fromUser: Boolean, position: Location) {
        if (fromUser) {
            isDiscoverVisible as MutableLiveData
            isDiscoverVisible.value = true
        }
    }

    fun onMapClicked(location: Location) {
        val pois = ArrayList(pois.value!!).map {
            Poi(
                it.codeEtab,
                it.name,
                it.activity,
                it.visual,
                it.pin_carto,
                it.longitude,
                it.latitude,
                false
            )
        }
        (this.pois as MutableLiveData).value = pois
    }

    fun discoverClicked() {
        (this.isDiscoverVisible as MutableLiveData).value = false
        (this.pois as MutableLiveData).value = mockResultWithoutQuestion().map {
            Poi(
                (0..1000000).random().toString(),
                it.name,
                it.activity,
                it.visual,
                it.pin_carto,
                it.longitude,
                it.latitude,
                false
            )
        }
    }

    fun bottomCardClicked() {
        router.toDetailedSheet(selectedPoi.value!!.codeEtab)
    }
}