package com.example.mappoc.scenes.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.mappoc.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

}