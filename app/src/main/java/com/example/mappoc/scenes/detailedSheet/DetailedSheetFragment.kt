package com.example.mappoc.scenes.detailedSheet

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.navArgs
import com.example.mappoc.R
import com.example.mappoc.databinding.DetailedSheetViewBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class DetailedSheetFragment : BottomSheetDialogFragment(), DialogInterface.OnShowListener {

    val navArgs: DetailedSheetFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<DetailedSheetViewBinding>(inflater, R.layout.detailed_sheet_view, container, false).apply {
            viewModel = DetailedSheetViewModel(navArgs.id)
        }

        return binding.root
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            setOnShowListener(this@DetailedSheetFragment)
        }
    }

    override fun onShow(dialog: DialogInterface?) {
        dialog as BottomSheetDialog
        val layout: FrameLayout = dialog.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
        BottomSheetBehavior.from(layout).state = BottomSheetBehavior.STATE_EXPANDED
    }
}