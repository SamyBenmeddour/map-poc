package com.example.mappoc.scenes.localmap

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.navGraphViewModels
import com.example.mappoc.R
import com.example.mappoc.databinding.LocalMapViewBinding
import com.example.mappoc.ui.views.map.implementation.GoogleMapAdapter
import com.example.mappoc.ui.views.map.router.DefaultMapRouterAdapter

class LocalMapFragment : Fragment() {

    private val viewModel: LocalMapViewModel by navGraphViewModels(R.id.nav_graph)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val binding: LocalMapViewBinding = DataBindingUtil.inflate<LocalMapViewBinding>(inflater, R.layout.local_map_view, container, false)
            .apply {
                this.mapView.displayMap(GoogleMapAdapter(requireContext(), this@LocalMapFragment))
                this.lifecycleOwner = this@LocalMapFragment
                this.viewmodel = LocalMapViewModel(DefaultMapRouterAdapter(this@LocalMapFragment))
            }

        return binding.root
    }


}

fun mockResultWithoutQuestion(): List<Poi> = listOf(
    Poi(
        "59321618",
        "ASSOCIATIONS CULTURELLES, DE LOISIRS",
        "loisir",
        "",
        "",
        2.255729,
        48.833195
    ),
    Poi(
        "01536241",
        "Cap.Nemo",
        "commerce",
        "https://www.cd.pagesjaunes.fr/media/ugc/cap_nemo_09207300_165533664",
        "",
        2.2208,
        48.8668
    ),
    Poi(
        "66102500",
        "Nemo Jean",
        "ecole",
        "",
        "",
        2.264334,
        48.847834
    ),
    Poi(
        "56486211",
        "Memo Voyage",
        "tourisme",
        "",
        "",
        2.434354,
        48.936011
    ),
    Poi(
        "57114091",
        "Médiathèque La Mémo",
        "bibliotheque",
        "",
        "",
        2.061108,
        49.060659,
        false
    ),
    Poi(
        "571",
        "Médiathèque La Mémo",
        "bibliotheque",
        "",
        "",
        2.161108,
        49.060659,
        false
    ),
    Poi(
        "57114092",
        "Médiathèque La Mémo",
        "bibliotheque",
        "",
        "",
        2.261108,
        49.260659,
        false
    ),
    Poi(
        "57114093",
        "Médiathèque La Mémo",
        "bibliotheque",
        "",
        "",
        2.361108,
        49.360659,
        false
    ),
    Poi(
        "57114094",
        "Médiathèque La Mémo",
        "bibliotheque",
        "",
        "",
        2.461108,
        49.460659,
        false
    ),
    Poi(
        "57114095",
        "Médiathèque La Mémo",
        "bibliotheque",
        "",
        "",
        2.561108,
        49.560659,
        false
    ),
    Poi(
        "57114096",
        "Médiathèque La Mémo",
        "bibliotheque",
        "",
        "",
        2.661108,
        49.660659,
        false
    ),
    Poi(
        "57114097",
        "Médiathèque La Mémo",
        "bibliotheque",
        "",
        "",
        2.761108,
        49.760659,
        false
    )
)
