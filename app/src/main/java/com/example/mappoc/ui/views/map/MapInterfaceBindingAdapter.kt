package com.example.mappoc.ui.views.map

import android.animation.Animator
import android.view.View
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import com.example.mappoc.scenes.localmap.Poi
import com.example.mappoc.scenes.localmap.PoiList

@BindingAdapter("onPoiClick")
fun MapView.onPoiClick(listener: PoiClickedListener) {
    this.setOnMarkerClickListener {
        val poi = Poi(
            it.id,
            "",
            "",
            "",
            "",
            it.location.longitude,
            it.location.latitude,
            it.isSelected
        )
        listener.onPoiClicked(poi)
    }
}

@BindingAdapter("onCameraMove")
fun MapView.onCameraMove(listener: CameraMoveListener) {
    this.setOnCameraMoveListener { fromUser, location ->
        listener.onMove(fromUser, location)
    }
}

@BindingAdapter("onMapClick")
fun MapView.onMapClick(listener: OnMapClickListener) {
    this.setOnMapClickListener {
        listener.onClick(it)
    }
}

@BindingAdapter("pois")
fun MapView.pois(pois: PoiList) {
    this.addMarkers(
        pois.map { Marker(it.codeEtab, Location(it.latitude, it.longitude), it.name, null, it.isSelected) }
    )
}

@BindingAdapter("isVisible")
fun View.isVisible(visible: Boolean) {
    this.isVisible = visible
}

@BindingAdapter("isBottomCardVisible")
fun View.isBottomCardVisible(visible: Boolean) {
    this.animate()
        .translationY( if (!visible) this.height.toFloat() else 0f )
        .setListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {
            }

            override fun onAnimationEnd(animation: Animator?) {
                isVisible = visible
            }

            override fun onAnimationCancel(animation: Animator?) {
            }

            override fun onAnimationStart(animation: Animator?) {
                if (visible) {
                    this@isBottomCardVisible.isVisible = true
                }
            }
        })
}

interface CameraMoveListener {
    fun onMove(fromUser: Boolean, location: Location)
}

interface OnMapClickListener {
    fun onClick(location: Location)
}

interface PoiClickedListener {
    fun onPoiClicked(poi: Poi)
}
