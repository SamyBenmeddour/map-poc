package com.example.mappoc.ui.views.map.implementation.cluster

import android.content.Context
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem
import com.google.maps.android.clustering.ClusterManager
import kotlin.random.Random

class PoiClusterManager(context: Context, googleMap: GoogleMap) : ClusterManager<PoiCluster>(context, googleMap)

data class PoiCluster(val id: String,
                      private val position: LatLng,
                      private val title: String,
                      private val snippet: String,
                      val isSelected: Boolean,
                      val isFavorite: Boolean) : ClusterItem {


    override fun getSnippet() = snippet
    override fun getTitle() = title
    override fun getPosition() = position


    override fun hashCode(): Int {
        return Random(10000).nextInt()
    }

    override fun equals(other: Any?): Boolean {
        return false
    }

}