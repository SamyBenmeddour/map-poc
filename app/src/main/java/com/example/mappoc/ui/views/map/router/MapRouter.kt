package com.example.mappoc.ui.views.map.router

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.mappoc.scenes.localmap.LocalMapFragmentDirections

interface MapRouter {
    fun toDetailedSheet(codeEtab: String)
}

internal class DefaultMapRouterAdapter(fragment: Fragment) : MapRouter {

    private val navigationController = fragment.findNavController()

    override fun toDetailedSheet(codeEtab: String) {
        val direction = LocalMapFragmentDirections.actionMapToDetailedSheet(codeEtab)
        navigationController.navigate(direction)
    }

}

