package com.example.mappoc.ui.views.map

enum class Icon {
    NOT_SELECTED,
    SELECTED
}
data class Marker (
    val id: String,
    val location: Location,
    val name: String,
    val icon: Icon?,
    val isSelected: Boolean
)

data class Location (val latitude: Double, val longitude: Double)