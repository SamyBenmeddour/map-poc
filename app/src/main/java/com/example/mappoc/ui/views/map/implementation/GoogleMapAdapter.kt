package com.example.mappoc.ui.views.map.implementation

import android.content.Context
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import com.example.mappoc.ui.views.map.*
import com.example.mappoc.ui.views.map.Map
import com.example.mappoc.ui.views.map.implementation.cluster.PoiCluster
import com.example.mappoc.ui.views.map.implementation.cluster.PoiClusterManager
import com.example.mappoc.ui.views.map.implementation.cluster.PoiMarkerClusterRenderer
import com.google.android.gms.maps.*
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import kotlin.time.ExperimentalTime


class GoogleMapAdapter (private val context: Context, private val owner: LifecycleOwner) : Map, LifecycleObserver, OnMapReadyCallback {

    private val googleMapView: MapView = MapView(context).also {
        it.getMapAsync(this)
    }

    private var markerClickListener: OnMarkerClickListener? = null
    private var cameraMoveListener: OnCameraMoveListener? = null
    private var onMapClickListener: OnMapClickListener? = null

    private lateinit var clusterManager: PoiClusterManager

    override fun addMarker(marker: Marker) {
        this.clusterManager.addItem( PoiCluster(marker.id, LatLng(marker.location.latitude, marker.location.longitude), marker.name, marker.name, marker.isSelected, marker.isSelected) )
    }

    private fun GoogleMap.addPois(pois: List<PoiCluster>) {
        clusterManager.clearItems()
        clear()
        clusterManager.cluster()
        if (pois.isEmpty()) {
            return
        }
        val builder = LatLngBounds.Builder()
        pois.forEach {
            builder.include(LatLng(it.position.latitude, it.position.longitude))
            clusterManager.addItem(it)
        }
        clusterManager.cluster()
        updateCameraPosition(builder.build())
    }

    private fun Marker.toPoiCluster(): PoiCluster {
        return PoiCluster(this.id, LatLng(this.location.latitude, this.location.longitude), this.name, this.name, this.isSelected, false)
    }

    private fun List<Marker>.getChangedMarkers(otherList: List<Marker>): List<Marker> {
        return otherList.filter { newMarker ->
            this.find { it.id == newMarker.id && newMarker.isSelected != it.isSelected } != null
        }
    }

    private fun Marker.changeIcon(icon: Icon) {
        val newIcon = (clusterManager.renderer as PoiMarkerClusterRenderer).getIcon(this)

        clusterManager.markerCollection
            .markers
            .find {
                it.title == this.id
            }
            ?.apply {
                setIcon(newIcon)
                tag = icon == Icon.SELECTED
            }
    }
    private fun updateChangedMarkers(markers: List<Marker>) {
        markers.forEach {
            it.changeIcon(if (it.isSelected) Icon.SELECTED else Icon.NOT_SELECTED)
            if (it.isSelected) {
                googleMapView.getMapAsync { map ->
                    map.zoomIn(LatLng(it.location.latitude, it.location.longitude))
                }
            }
        }
    }

    @ExperimentalTime
    override fun addMarkers(markers: List<Marker>) {
        this.googleMapView.getMapAsync { googleMap ->
            val currentMarkers = clusterManager.markerCollection
                .markers
                .let { it.map { it.toUiMarker() } }

            val isJustUIUpdate = currentMarkers.areSameMarkers(markers)

            if (isJustUIUpdate) {
                val updatedMarkers = currentMarkers.getChangedMarkers(markers)
                updateChangedMarkers(updatedMarkers)
            } else {
                googleMap.addPois(markers.map { it.toPoiCluster() })
            }

        }
    }

    private fun updateCameraPosition(latLng: LatLng) {
        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17.0f)
        animateCamera(cameraUpdate)
    }

    private fun updateCameraPosition(latLngBounds: LatLngBounds) {
        val cameraUpdate = CameraUpdateFactory.newLatLngBounds(latLngBounds, 40)
        animateCamera(cameraUpdate)
    }
    private fun animateCamera(cameraUpdate: CameraUpdate?) {
        this.googleMapView.getMapAsync { googleMap ->
            googleMap.animateCamera(cameraUpdate)
        }
    }

    override fun clearMarkers() {
        this.googleMapView.getMapAsync { googleMap ->
            googleMap.clear()
            this.clusterManager.clearItems()
        }
    }

    override fun setOnMarkerClickListener(listener: OnMarkerClickListener) {
        this.markerClickListener = listener
    }

    override fun setOnMapClickListener(listener: OnMapClickListener) {
        this.onMapClickListener = listener
    }

    override fun onViewReady() {
        this.owner.lifecycle.addObserver(this)
    }

    override fun getView(): View {
        return this.googleMapView
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.clusterManager = PoiClusterManager(context, googleMap)
        this.clusterManager.renderer = PoiMarkerClusterRenderer(context, googleMap, this.clusterManager)

        googleMap.setOnCameraMoveStartedListener {
            this.cameraMoveListener?.onStart(it == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE, googleMap.cameraPosition.target.run { Location(latitude, longitude) })
        }

        googleMap.setOnCameraMoveCanceledListener {
            this.cameraMoveListener?.onEnd()
        }

        googleMap.setOnCameraIdleListener {
            this.cameraMoveListener?.onEnd()
        }

        googleMap.setOnCameraMoveListener {
        }

        googleMap.setOnMapClickListener {
            this.onMapClickListener?.onClick(Location(it.latitude, it.longitude))
        }

        googleMap.setOnCameraIdleListener(this.clusterManager)
        googleMap.setOnMarkerClickListener(this.clusterManager)


        this.clusterManager.setOnClusterItemClickListener {
            this.markerClickListener?.onClick( Marker(it.id, Location(it.position.latitude, it.position.longitude), it.title, null, false))
            true
        }
    }



    private fun List<Marker>.areSameMarkers(otherList: List<Marker>): Boolean {
        if (this.size != otherList.size) {
            return false
        }
        val current = this.map { it.id }
        val other = otherList.map { it.id }

        return current.containsAll(other)
    }

    private fun GoogleMap.zoomIn(position: LatLng) {
        this.animateCamera(CameraUpdateFactory.newLatLng(position))
    }

    private fun GoogleMapMarker.toUiMarker(): Marker {
        val location = Location(this.position.latitude, this.position.longitude)
        return Marker(this.title, location, this.title, null, (this.tag as? Boolean) ?: false)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        googleMapView.onCreate(null)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        googleMapView.onStart()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        googleMapView.onResume()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        googleMapView.onPause()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        googleMapView.onStop()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        googleMapView.onDestroy()
    }

    override fun setOnCameraMoveListener(listener: OnCameraMoveListener) {
        this.cameraMoveListener = listener
    }
}

private typealias GoogleMapMarker = com.google.android.gms.maps.model.Marker