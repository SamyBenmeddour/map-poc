package com.example.mappoc.ui.views.map.implementation.cluster

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.mappoc.R
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.google.maps.android.ui.IconGenerator
import kotlinx.android.synthetic.main.cluster_view.view.*


class PoiMarkerClusterRenderer(private val context: Context,
                               googleMap: GoogleMap,
                               clusterManager: ClusterManager<PoiCluster>) : DefaultClusterRenderer<PoiCluster>(context, googleMap, clusterManager) {

    companion object {
        private const val MIN_CLUSTER_SIZE = 5
    }


    override fun shouldRenderAsCluster(cluster: Cluster<PoiCluster>?): Boolean {
        return (cluster?.size ?: 0 >= MIN_CLUSTER_SIZE)
    }

    override fun onBeforeClusterItemRendered(item: PoiCluster, markerOptions: MarkerOptions) {
        val view = LayoutInflater.from(context).inflate(if (item.isSelected) R.layout.poi_selected else R.layout.poi_normal, null, false)
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(view.bitmap))
        markerOptions.title(item.id)
    }

    override fun onBeforeClusterRendered(cluster: Cluster<PoiCluster>, markerOptions: MarkerOptions) {
        cluster.let {
            val clusterIconGenerator = IconGenerator(context.applicationContext).also {
                it.setBackground(null)
            }
            val myInflater = LayoutInflater.from(context)
            val clusterView = myInflater.inflate(R.layout.cluster_view, null, false)
            clusterView.text.text = cluster.size.toString()
            clusterIconGenerator.setContentView(clusterView)
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(clusterIconGenerator.makeIcon()))
        }

    }

    override fun onClusterItemRendered(clusterItem: PoiCluster, marker: Marker) {
        marker.tag = clusterItem.isSelected
        super.onClusterItemRendered(clusterItem, marker)
    }



    fun getIcon(marker: com.example.mappoc.ui.views.map.Marker): BitmapDescriptor {
        val view = LayoutInflater.from(context).inflate(if (marker.isSelected) R.layout.poi_selected else R.layout.poi_normal, null, false)
        return BitmapDescriptorFactory.fromBitmap(view.bitmap)
    }

    fun selectMarker(marker: Marker) {
        val view = LayoutInflater.from(context).inflate(R.layout.poi_selected, null, false)
        marker.setIcon(BitmapDescriptorFactory.fromBitmap(view.bitmap))
    }

    fun unSelectMarker(marker: Marker) {
        val view = LayoutInflater.from(context).inflate(R.layout.poi_normal, null, false)
        marker.setIcon(BitmapDescriptorFactory.fromBitmap(view.bitmap))
    }

    private val View.bitmap: Bitmap
    get() {
        measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val b = Bitmap.createBitmap(measuredWidth, measuredHeight, Bitmap.Config.ARGB_8888)
        val c = Canvas(b)
        layout(0, 0, measuredWidth, measuredHeight)
        draw(c)
        return b
    }
}