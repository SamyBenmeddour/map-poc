package com.example.mappoc.ui.views.map

import android.view.View

interface Map {

    fun addMarker(marker: Marker)

    fun addMarkers(markers: List<Marker>)

    fun clearMarkers()

    fun setOnMarkerClickListener(listener: OnMarkerClickListener)

    fun setOnCameraMoveListener(listener: OnCameraMoveListener)

    fun setOnMapClickListener(listener: OnMapClickListener)

    fun onViewReady()

    fun getView(): View

}