package com.example.mappoc.ui.views.map

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.FrameLayout

interface OnMarkerClickListener {

    fun onClick(marker: Marker)

}
interface OnCameraMoveListener {

    fun onStart(fromUser: Boolean, location: Location)

    fun onEnd()

}

class MapView @JvmOverloads constructor (context: Context,
                                         attrs: AttributeSet? = null,
                                         defStyleAttr: Int = 0) : FrameLayout(context, attrs, defStyleAttr) {

    private lateinit var mapInterface: Map

    fun displayMap (mapInterface: Map) {
        this.mapInterface = mapInterface
        this.mapInterface.onViewReady()
        this.mapInterface.getView().also {
            it.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            this.addView(it)
        }
    }

    fun addMarkers(markers: List<Marker>) {
        this.mapInterface.addMarkers(markers)
    }

    fun clearMarkers() {
        this.mapInterface.clearMarkers()
    }

    fun setOnMarkerClickListener(listener: OnMarkerClickListener) {
        mapInterface.setOnMarkerClickListener(listener)
    }

    fun setOnMarkerClickListener(listener: (Marker) -> Unit) {
        mapInterface.setOnMarkerClickListener(object : OnMarkerClickListener {
            override fun onClick(marker: Marker) {
                listener.invoke(marker)
            }}
        )
    }

    fun setOnCameraMoveListener(listener: OnCameraMoveListener) {
        mapInterface.setOnCameraMoveListener(listener)
    }

    fun setOnCameraMoveListener(listener: (Boolean, Location) -> Unit) {
        mapInterface.setOnCameraMoveListener(object : OnCameraMoveListener {
            override fun onStart(fromUser: Boolean, location: Location) {
                listener.invoke(fromUser, location)
            }

            override fun onEnd() {}
        })
    }

    fun setOnMapClickListener(listener: (Location) -> Unit) {
        mapInterface.setOnMapClickListener(object: OnMapClickListener {
            override fun onClick(location: Location) {
                listener.invoke(location)
            }
        })
    }

}